<?php

use EasyWeChat\Factory;

function route_class()
{
    return str_replace('.', '-', Route::currentRouteName());
}

function getOfficialAccount(){
    $config = [
        'app_id' => config('app.WECHAT_APP_ID'),
        'secret' => config('app.WECHAT_APP_SECRET'),
        'token' => config('app.WECHAT_APP_TOKEN'),
        'response_type' => 'array'
    ];

    return Factory::officialAccount($config);
}
