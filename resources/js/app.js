/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// 在入口文件中（main.js），导入组件库
import vueHashCalendar from 'vue-hash-calendar'
// 引入组件CSS样式
import 'vue-hash-calendar/lib/vue-hash-calendar.css'

// 注册组件库
Vue.use(vueHashCalendar)

const app = new Vue({
    el: '#app',
    data : {
        show : false,
        model : 'dialog',
        datetime : ''
    },
    created : function(){
        if ($('input[name="booking_time"]').data('value')){
            this.datetime = $('input[name="booking_time"]').data('value');
        }
    },
    methods : {
        confirmEvent : function(val){
            this.datetime = val;
        },
        datetimePicker : function(){
            this.show = !this.show;
        }
    }
});
