@extends('layouts.app')
@section('content')
    <form method="post" action="{{ route('Booking.store') }}" accept-charset="UTF-8" enctype="multipart/form-data" >
        {{ csrf_field() }}
        @include('shared._error')
        <div class="form-row">
            <label style="padding:10px 0px 0px 0px;"><span class="required-highlight">*</span>进仓单：</label>
            <div class="custom-file mb-3">
                <input type="file" name="files" class="custom-file-input" lang="zh" id="validatedCustomFile">
                <label class="custom-file-label" for="validatedCustomFile">进仓单照片一车多票</label>
            </div>
            <div class="form-group col-md-12 col-lg-12 col-sm-12" style="top: 4px;">
                <span class="placeholder-required-highlight" style="left: 9px;">*</span>
                <input type="text" name="name" class="form-control" placeholder="姓名">
            </div>
        </div>
        <div class="form-group">
            <span class="placeholder-required-highlight">*</span>
            <input type="text" name="phone" class="form-control" placeholder="联系电话">
        </div>
        <div class="form-group">
            <span class="placeholder-required-highlight">*</span>
            <input type="text" name="id_card" class="form-control" placeholder="身份证号码">
        </div>
        <div class="form-group">
            <span class="placeholder-required-highlight">*</span>
                <input type="text" name="booking_time" v-model="datetime" class="form-control" readonly @click="datetimePicker" placeholder="预约时间">
        </div>
        <div class="form-group">
            <span class="placeholder-required-highlight">*</span>
            <input type="text" class="form-control" name="plate_number" placeholder="车牌号码">
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-block fixed-bottom" style="z-index: 500">提交</button>
        <vue-hash-calendar :visible.sync="show" :model="model" @confirm="confirmEvent" format="YY-MM-DD hh:mm"></vue-hash-calendar>
    </form>
@stop

