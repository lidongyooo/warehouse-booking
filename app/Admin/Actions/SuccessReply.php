<?php
namespace App\Admin\Actions;
use Encore\Admin\Admin;

class SuccessReply
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function script()
    {
        return <<<SCRIPT
$('.success-reply').on('click', function () {

    Swal.fire({
        type: 'warning', // 弹框类型
        title: '确认回复吗', //标题
        confirmButtonText: '确定',// 确定按钮的 文字
        showCancelButton: true, // 是否显示取消按钮
        cancelButtonText: "取消", // 取消按钮的 文字
    }).then((isConfirm) => {
            //判断 是否 点击的 确定按钮
            if (isConfirm.value) {
                   $.ajax({
                        url:'/Booking/successReply',
                        method:'post',
                        data:{"id":$(this).data('id'),'_token': $('meta[name=csrf-token]').attr("content")},
                        dataType : 'json',
                        success:function(res){
                            Swal.fire({
                                title: res.msg,
                            }).then((ifConfirm) => {
                                $('.input-group-btn .btn-default').click();
                            })
                         }
                   });
            }
    
    });

});

SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-success success-reply' data-id='{$this->id}'>成功回复</a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}
