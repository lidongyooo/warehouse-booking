<?php
namespace App\Admin\Actions;
use Encore\Admin\Admin;

class FailReply
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function script()
    {
        return <<<SCRIPT
$(function(){
    $('.grid-table').viewer();
})
$('.fail-reply').on('click', function () {

    Swal.fire({
      title: '请输入原因',
      input: 'text',
      showCancelButton: true,
      confirmButtonText: '发送',// 确定按钮的 文字
      cancelButtonText: "取消", // 取消按钮的 文字
    }).then((isConfirm) => {
            //判断 是否 点击的 确定按钮
            if (isConfirm.value) {
                   $.ajax({
                        url:'/Booking/failReply',
                        method:'post',
                        data:{"id":$(this).data('id'),content:isConfirm.value,'_token': $('meta[name=csrf-token]').attr("content")},
                        dataType : 'json',
                        success:function(res){
                            Swal.fire({
                                title: res.msg,
                            }).then((ifConfirm) => {
                                $('.input-group-btn .btn-default').click();
                            })
                         }
                   });
            }
    
    });

});

SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-danger fail-reply' style='margin-left: 5px;' data-id='{$this->id}'>失败回复</a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}
