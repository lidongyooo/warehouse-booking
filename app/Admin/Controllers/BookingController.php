<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Restore;
use App\Booked;
use Encore\Admin\Actions\Response;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Grid;
use App\Admin\Actions\SuccessReply;
use App\Admin\Actions\FailReply;
use Encore\Admin\Grid\Displayers\Actions;
use Illuminate\Http\Request;

class BookingController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '预约';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Booked());

        $grid->filter(function ($filter) {

            // 范围过滤器，调用模型的`onlyTrashed`方法，查询出被软删除的数据。
            $filter->scope('trashed', '回收站')->onlyTrashed();

        });

        $grid->column('id', 'Id')->sortable();
        $grid->model()->orderBy('id','desc');

        $grid->column('name', '姓名');
        $grid->column('phone', '手机号码');
        $grid->column('id_card', '身份证');
        $grid->column('booking_date', '预约日期');
        $grid->column('time_range', '预约时间');
        $grid->column('plate_number', '车牌号码');

        $index = 0;
       // 显示多图
        $grid->column('files','附件')->display(function ($files){
            global $index;
            $img = '';
            $imgs = explode(',',$files);
            foreach ($imgs as $v){
                $img .= '<img data-index="'.$index.'"  style="width: 50px;height: 50px;" data-original="'.$v.'" alt="点击查看" class="image" src="'.$v.'"/>';
                $index++;
            }
            return $img;
        });

        $grid->column('is_reply','回复状态')->style('font-weight:700;')->filter([
            0 => '未回复',
            1 => '成功回复',
            2 => '失败回复',
        ])->display(function ($value){
            switch ($value){
                case 1:
                    return '成功回复';
                    break;
                case 2:
                    return '失败回复';
                    break;
                default :
                    return '未回复';
            };
        });

        $grid->column('created_at', '创建时间');

        $grid->export(function ($export) {
            $export->except(['files']);
        });

        $grid->disableCreateButton();

        // 最原始的`按钮图标`形式
        $grid->setActionClass(Actions::class);

        $grid->actions(function ($actions) use($grid) {
            $actions->disableView();
            $actions->disableEdit();
            if (\request('_scope_') == 'trashed') {
                $actions->append(new Restore($actions->getkey()));
                $actions->disableDelete();
                $grid->tools(function ($tools) {
                    $tools->batch(function ($batch) {
                        $batch->disableDelete();
                    });
                });
            }else{
                $actions->append(new SuccessReply($actions->getkey()));
                $actions->append(new FailReply($actions->getkey()));
            }
        });

        $grid->quickSearch('name', 'phone', 'id_card', 'plate_number', 'booking_date', 'time_range');

        return $grid;
    }

    public function destroy($id)
    {
        $ids = explode(',',$id);
        try{

            Booked::destroy($ids);

            return response()->json([
                'status' => 1,
                'message' => '删除成功'
            ]);

        } catch (\Exception $e){

            return response()->json([
                'status' => 0,
                'message' => '删除异常'
            ]);
        }

    }

    public function restore(Request $request)
    {
        $id = $request->input('id');
        Booked::withTrashed()
            ->where('id', $id)
            ->restore();
        return response()->json([
            'msg' => '恢复成功'
        ]);
    }

}
