<?php
namespace App\Handlers;
use Illuminate\Support\Str;
use EasyWeChat\Kernel\Support\File;
use OSS\OssClient;
use OSS\Core\OssException;

class ImageUploadHandler
{

    // 只允许以下后缀名的图片文件上传
    protected $allowed_ext = ["png", "jpg", "gif", 'jpeg'];
    public function save($file, $folder, $file_prefix = 0)
    {
        // 构建存储的文件夹规则，值如：uploads/images/avatars/201709/21/
        // 文件夹切割能让查找效率更高。
        $folder_name = "uploads/images/$folder";
        // 文件具体存储的物理路径，`public_path()` 获取的是 `public` 文件夹的物理路径。
        // 值如：/home/vagrant/Code/larabbs/public/uploads/images/avatars/201709/21/
        $upload_path = public_path() . '/' . $folder_name;
        // 获取文件的后缀名，因图片从剪贴板里黏贴时后缀名为空，所以此处确保后缀一直存在
        $extension = strtolower($file->getClientOriginalExtension()) ?: 'png';
        // 拼接文件名，加前缀是为了增加辨析度，前缀可以是相关数据模型的 ID
        // 值如：1_1493521050_7BVc9v9ujP.png
        $filename = $file_prefix . '_' . time() . '_' . mt_rand(100000000,9999999999) . '.' . $extension;
        // 如果上传的不是图片将终止操作
        if ( ! in_array($extension, $this->allowed_ext)) {
            return false;
        }
        // 将图片移动到我们的目标存储路径中
        $file->move($upload_path, $filename);
        return [
            'path' => config('app.url') . "/$folder_name/$filename"
        ];
    }

    public function wechatImageSave($file, $folder, $file_prefix = 0)
    {
        // 构建存储的文件夹规则，值如：uploads/images/avatars/201709/21/
        // 文件夹切割能让查找效率更高。
        $folder_name = "uploads/images/$folder";
        // 文件具体存储的物理路径，`public_path()` 获取的是 `public` 文件夹的物理路径。
        // 值如：/home/vagrant/Code/larabbs/public/uploads/images/avatars/201709/21/
        $upload_path = public_path() . '/' . $folder_name;
        // 值如：1_1493521050_7BVc9v9ujP.png
        $filename = $file_prefix . '_' . time() . '_' . mt_rand(100000000,9999999999);
        // 如果上传的不是图片将终止操作

        // 将图片移动到我们的目标存储路径中
        $app = getOfficialAccount();
        $stream = $app->media->get($file);
        if ($stream instanceof \EasyWeChat\Kernel\Http\StreamResponse) {
            // 自定义文件名，不需要带后缀
            $filename = $stream->saveAs($upload_path, $filename);
        }
//        file_put_contents("$folder_name/$filename",base64_decode($match_result[2]));
        return [
            'path' => config('app.url') . "/$folder_name/$filename"
        ];
    }

    public function wechatImageSaveToOss($file, $folder, $file_prefix = 0)
    {
        $folder_name = "images/$folder/".date('Y/m/');
        $filename = $file_prefix . '_' . time() . '_' . mt_rand(100000000,9999999999);

        $app = getOfficialAccount();
        $stream = $app->media->get($file);
        $result = [];
        if ($stream instanceof \EasyWeChat\Kernel\Http\StreamResponse) {
            $contents = $stream->getBodyContents();
            $fileExt = File::getStreamExt($contents);
            $filename .= $fileExt;

            $oss = new OssClient(config('other.oss.access_key_id'), config('other.oss.access_key_secret'), config('other.oss.endpoint'));
            $result = $oss->putObject(config('other.oss.bucket'),$folder_name.$filename,$contents);
        }

        if(isset($result['info'])){
            return [
                'path' => $result['info']['url']
            ];
        }else{
            throw new Exception("文件上传错误！");
        }

    }


}
