<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booked extends Model
{
    use DefaultDatetimeFormat;
    use SoftDeletes;

    protected $fillable = [
      'name','phone','id_card','booking_date','time_range','plate_number','files','user_id','is_reply'
    ];
}
