<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        //扩展身份证验证规则
        Validator::extend('identitycards', function($attribute, $value, $parameters) {
            return preg_match(' /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/', $value);
        });
        Validator::extend('telphone', function($attribute, $value, $parameters) {
            return preg_match('/^1[34578][0-9]{9}$/', $value);
        });
        Validator::extend('platenumber', function($attribute, $value, $parameters) {
            return preg_match('/^(([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z](([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z][A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳使领]))$/u', $value);
        });

    }
}
