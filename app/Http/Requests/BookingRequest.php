<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required|telphone',
            'id_card' => 'required|identitycards',
            'booking_date' => 'required',
            'plate_number' => 'required|platenumber',
            'files' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'phone.telphone' => '请输入合法手机号',
            'id_card.identitycards' => '请输入合法身份证',
            'plate_number.platenumber' => '请输入合法车牌'
        ];
    }

    public function attributes()
    {
        return [
            'name' => '姓名',
            'phone' => '联系电话',
            'id_card' => '身份证',
            'booking_time' => '预约时间',
            'plate_number' => '车牌号码',
            'files' => '附件'
        ];
    }
}
