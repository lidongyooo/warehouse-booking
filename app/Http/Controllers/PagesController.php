<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;

class PagesController extends Controller
{
    public function root()
    {
        return view('pages.root');
    }
}
