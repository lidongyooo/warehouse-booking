<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('姓名');
            $table->string('phone')->comment('电话');
            $table->string('id_card')->comment('身份证');
            $table->string('booking_date')->comment('预约日期');
            $table->string('time_range')->comment('预约时间区间');
            $table->string('plate_number')->comment('车牌号码');
            $table->text('files')->nullable()->comment('文件');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookeds');
    }
}
