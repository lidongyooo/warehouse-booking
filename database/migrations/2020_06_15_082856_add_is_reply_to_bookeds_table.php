<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsReplyToBookedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookeds', function (Blueprint $table) {
            $table->tinyInteger('is_reply')->default(0)->comment('0未回复，1已回复');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookeds', function (Blueprint $table) {
            $table->dropColumn('is_reply');
        });
    }
}
