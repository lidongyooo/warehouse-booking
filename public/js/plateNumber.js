var provinces = new Array("京","沪","浙","苏","粤","鲁","晋","冀",
            "豫","川","渝","辽","吉","黑","皖","鄂",
            "津","贵","云","桂","琼","青","新","藏",
            "蒙","宁","甘","陕","闽","赣","湘");

var keyNums = new Array("0","1","2","3","4","5","6","7","8","9",
            "Q","W","E","R","T","Y","U","I","O","P",
            "A","S","D","F","G","H","J","K","L",
            "确认","Z","X","C","V","B","N","M","删除");
var plateNumer = [];
var elem = $('input[name="plate_number"]');
var next=0;
	function showProvince(){
			$("#pro").html("");
			var ss="";
			for(var i=0;i<provinces.length;i++){
				ss=ss+addKeyProvince(i)
			}
			$("#pro").html("<ul class='clearfix ul_pro'>"+ss+"<li class='li_close' onclick='closePro();'><span>关闭</span></li><li class='li_clean' onclick='cleanPro();'><span>清空</span></li></ul>");
	}
	function showKeybord(){
			$("#pro").html("");
			var sss="";
			for(var i=0;i<keyNums.length;i++){
				sss=sss+'<li class="ikey ikey'+i+' '+(i>9?"li_zm":"li_num")+' '+(i>28?"li_w":"")+'" ><span onclick="choosekey(this,'+i+');">'+keyNums[i]+'</span></li>'
			}
			$("#pro").html("<ul class='clearfix ul_keybord'>"+sss+"</ul>");
	}
    function addKeyProvince(provinceIds){
        var addHtml = '<li>';
            addHtml += '<span onclick="chooseProvince(this);">'+provinces[provinceIds]+'</span>';
            addHtml += '</li>';
            return addHtml;
    }

    function chooseProvince(obj){
	    plateNumer.push($(obj).text());
	    setValue()
       $(".input_pro span").text($(obj).text());
	   $(".input_pro").addClass("hasPro");
	   $(".input_pp").find("span").text("");
       $(".ppHas").removeClass("ppHas");
	   next=0;
	   showKeybord();
	}


	function choosekey(obj,jj){
		if(jj==29){
            reg();
            layer.closeAll();
		}else if(jj==37){
			if($(".ppHas").length==0){
                $(".hasPro").find("span").text("");
				$(".hasPro").removeClass("hasPro");
				if(next == 0){
                    showProvince();
                }
			}
			$(".ppHas:last").find("span").text("");
			$(".ppHas:last").removeClass("ppHas");
			plateNumer.splice(next,1);
			setValue()
			next=next-1;
			if(next<1){
				next=0;
			}
		}else{
			for(var i = 0; i<$(".input_pp").length;i++){
				if(next==0 & jj<10 & $(".input_pp:eq("+next+")").hasClass("input_zim")){
					layer.open({
						content: '车牌第二位为字母',
						skin: 'msg',
						time: 1
					});
					return
				}
                plateNumer.push($(obj).text());
				setValue()
				$(".input_pp:eq("+next+")").find("span").text($(obj).text());
				$(".input_pp:eq("+next+")").addClass("ppHas");
				next=next+1;

				getpai();
				return
			}

		}
	}
	function closePro(){
        reg();
       layer.closeAll()
	}
	function cleanPro(){
       $(".ul_input").find("span").text("");
       $(".hasPro").removeClass("hasPro");
       $(".ppHas").removeClass("ppHas");
	   next=0;
	}
	function trimStr(str){return str.replace(/(^\s*)|(\s*$)/g,"");}
	function getpai(){
		var pai=trimStr($(".car_input").text());
		$(".car_input").attr("data-pai",pai);
	}
	function setValue(){
	    elem.val(plateNumer.join(''));
    }

    function reg(){
        let reg_plate_number = /^(([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z](([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z][A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳使领]))$/;
        if(!(reg_plate_number.test(elem.val())) && $('form').html().indexOf('alert-danger') < 0){
            append('请输入合法车牌');
        }else if((reg_plate_number.test(elem.val()))){
            $('.alert-danger').remove();
        }
    }

	elem.click(function(){
		 layer.open({
			type: 1
			,content: '<div id="pro"></div>'
			,anim: 'up'
			,shade :false
			,style: 'position:fixed; bottom:0; left:0; width: 100%; height: auto; padding:0; border:none;'
		  });
		 showProvince()
	})
	elem.click(function(){
		 if(plateNumer.length > 0){ // 如果已选择省份
			 layer.open({
				type: 1
				,content: '<div id="pro"></div>'
				,anim: 'up'
				,shade :false
				,style: 'position:fixed; bottom:0; left:0; width: 100%; height: auto; padding:0; border:none;'
			  });
			 showKeybord()
		 }else{
			 $(".input_pro").click()
		 }
	})



