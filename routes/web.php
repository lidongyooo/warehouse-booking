<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','WechatController@wechatUserLogin')->name('Wechat.wechatUserLogin');
Route::get('/Wechat/user','WechatController@getUserInfo')->name('Wechat.user');
Route::get('/Wechat/response','WechatController@response')->name('Wechat.response');

Route::resource('Booking','BookingController',['only'=>['store','create']]);
Route::post('Booking/successReply','BookingController@successReply')->name('Booking.successReply');
Route::post('Booking/failReply','BookingController@failReply')->name('Booking.failReply');
